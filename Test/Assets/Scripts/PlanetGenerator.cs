﻿using Slartibartfast.Planets;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class PlanetGenerator : MonoBehaviour
{
    #region Private Fields

    [SerializeField]
    private UIResources resources = new UIResources();
    private Slartibartfast.PlanetGenerator generator;
    private Thread thread;

    [SerializeField]
    private Renderer sphereRenderer;

    #endregion Private Fields

    #region Public Methods

    public void GeneratePlanet()
    {
        Slartibartfast.Extensions.MathHelper.RandomSeed = resources.Seed.text.GetHashCode();

        DateTime time = DateTime.Now;

        using (generator = new Slartibartfast.PlanetGenerator())
        {
            //generator.BiomeColors = GetBiomeColors();
            try
            {
                generator.Sun = GetSun();
                generator.PlanetSettings = GetPlanet();
            }
            catch (Exception e)
            {
                Logger.Instance.Error(e.Message);
            }

            try
            {
                Planet p = generator.RunDebug();

                colorTex = generator.ColorTexture.ToUnityTexture();
                normalTex = GrayScaleToNormalMap.CreateDOT3(generator.HeightTexture.ToUnityTexture(), 10, 0);
                glossTex = generator.GlossTexture.ToUnityTexture();

                Material m = sphereRenderer.sharedMaterial;
                m.EnableKeyword("_NORMALMAP");
                m.EnableKeyword("_METALLICGLOSSMAP");
                m.SetTexture("_MainTex", colorTex);
                m.SetTexture("_MetallicGlossMap", glossTex);
                m.SetTexture("_BumpMap", normalTex);

                Logger.Instance.Print("Done. Time: " + (DateTime.Now - time));
            }
            catch (Exception e)
            {
                Logger.Instance.Error(e.Message);
            }
        }
    }

    private Texture2D colorTex;

    public Texture2D ColorTexture
    {
        get { return colorTex; }
        set { colorTex = value; }
    }

    private Texture2D normalTex;

    public Texture2D NormalTexture
    {
        get { return normalTex; }
        set { normalTex = value; }
    }

    private Texture2D glossTex;

    public Texture2D GlossTexture
    {
        get { return glossTex; }
        set { glossTex = value; }
    }


    #endregion Public Methods

    #region Private Methods

    private BiomeColors GetBiomeColors()
    {
        BiomeColors colors = new BiomeColors();

        return colors;
    }

    private PlanetSettings GetPlanet()
    {
        PlanetSettings settings = PlanetSettings.Earth();
        settings.DistanceToSun = float.Parse(resources.Planet_DistanceToSun.text, System.Globalization.CultureInfo.InvariantCulture);
        settings.TectonicPlatesCount = int.Parse(resources.Planet_TectonicPlateCount.text, System.Globalization.CultureInfo.InvariantCulture);

        Atmosphere atmosphere = new Atmosphere();
        atmosphere.PressureAtSealevel = float.Parse(resources.Planet_PressureAtSealevel.text, System.Globalization.CultureInfo.InvariantCulture);
        atmosphere.ScaleHeight = float.Parse(resources.Planet_ScaleHeight.text, System.Globalization.CultureInfo.InvariantCulture);

        settings.Atmosphere = this.resources.Planet_HasAtmosphere.isOn ? atmosphere : null;// new Atmosphere() { PressureAtSealevel = 0, ScaleHeight = 0 };

        return settings;
    }

    private Sun GetSun()
    {
        Sun sun = Sun.GetSol();
        sun.AbsoluteMagnitude = float.Parse(resources.Sun_AbsoluteMagnitude.text, System.Globalization.CultureInfo.InvariantCulture);
        sun.SpectralClass = (SpectralClass)resources.Sun_SpectralClass.value;
        return sun;
    }

    #endregion Private Methods

    #region Public Classes

    [Serializable]
    public class UIResources
    {
        public InputField Sun_AbsoluteMagnitude;
        public Dropdown Sun_SpectralClass;

        public InputField Planet_DistanceToSun;
        public InputField Planet_TectonicPlateCount;

        public InputField Planet_PressureAtSealevel;
        public InputField Planet_ScaleHeight;

        public Toggle Planet_HasAtmosphere;

        public InputField Seed;
    }

    #endregion Public Classes

    public void LoadSol()
    {
        resources.Sun_AbsoluteMagnitude.text = Sun.SOL_ABSOLUTE_MAGNITUDE.ToString();
        resources.Sun_SpectralClass.value = (int)Sun.SOL_SPECTRAL_CLASS;
    }

    private void Load(PlanetSettings settings)
    {
        resources.Planet_DistanceToSun.text = settings.DistanceToSun.ToString();
        resources.Planet_TectonicPlateCount.text = settings.TectonicPlatesCount.ToString();

        if (settings.Atmosphere != null)
        {
            resources.Planet_HasAtmosphere.interactable = resources.Planet_PressureAtSealevel.interactable = resources.Planet_ScaleHeight.interactable = true;
            resources.Planet_PressureAtSealevel.text = settings.Atmosphere.PressureAtSealevel.ToString();
            resources.Planet_ScaleHeight.text = settings.Atmosphere.ScaleHeight.ToString();
        }
        else
            resources.Planet_HasAtmosphere.interactable = resources.Planet_PressureAtSealevel.interactable = resources.Planet_ScaleHeight.interactable = false;
    }

    private void Start()
    {
        resources.Seed.text = 0.ToString();
    }

    public void LoadMars()
    {
        Load(PlanetSettings.Mars());
    }

    public void LoadVenus()
    {
        Load(PlanetSettings.Venus());
    }

    public void LoadEarth()
    {
        Load(PlanetSettings.Earth());
    }

    public void LoadMercury()
    {
        Load(PlanetSettings.Mercury());
    }
}