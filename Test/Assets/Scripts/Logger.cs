﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Logger : MonoBehaviour
{
    Text textElement;

    private static Logger instance;

    public static Logger Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        textElement = transform.GetChild(0).GetComponent<Text>();
    }

    public void Print(string text)
    {
        this.Color = Color.white;
        this.Text = text;
    }

    public void Error(string text)
    {
        this.Color = Color.red;
        this.Text = text;
    }

    public void Clear()
    {
        this.Color = Color.white;
        this.Text = string.Empty;
    }

    public Color Color
    {
        get { return textElement.color; }
        set { textElement.color = value; }
    }

    public string Text
    {
        get { return textElement.text; }
        set { textElement.text = value; }
    }
}
