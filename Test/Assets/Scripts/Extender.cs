﻿using UnityEngine;
using System.Collections;

public static class Extender
{
    public static Texture2D ToUnityTexture(this Slartibartfast.Textures.Texture tex)
    {
        int width = tex.Width;
        int height = tex.Height;
        Slartibartfast.Math.Color[] col = tex.Color;

        Texture2D unityTex = new Texture2D(width, height, TextureFormat.ARGB32, false);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Slartibartfast.Math.Color currentCol = col[y * width + x];
                unityTex.SetPixel(x, y, new Color(currentCol.R / 255f, currentCol.G / 255f, currentCol.B / 255f));
            }
        }

        unityTex.Apply();

        return unityTex;
    }
}